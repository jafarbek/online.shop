<?php

namespace app\controllers;

use app\models\HomashyoOvqatga;
use app\models\SearchForm;
use app\modules\admin\models\Homashyolar;
use app\modules\admin\models\Ovqatlar;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\rest\DeleteAction;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function beforeAction($action)
    {
        if (Yii::$app->authManager->getPermission(Yii::$app->controller->id . "/" . Yii::$app->controller->action->id)) {
            if (!Yii::$app->user->can(Yii::$app->controller->id . "/" . Yii::$app->controller->action->id)) {
                throw new ForbiddenHttpException(Yii::t('app', 'Access denied'));
            }
        }

        return parent::beforeAction($action);
    }
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $out = [];
        $model = new SearchForm();
        $ings = Homashyolar::find()->asArray()->all();
        $ovqatlarVaHom = [];
        $getData = Yii::$app->request->get();
        $ids = [];
            if (!empty($getData)) {
                $ids = $getData['SearchForm']['ings'];

                $ovqatlarVaHom = HomashyoOvqatga::find()
                    ->select(['ovqatlar.name', 'homashyo_ovqatga.ovqat_id', 'count(homashyolar.id) as ingCount'])
                    ->leftJoin('homashyolar', 'homashyolar.id = homashyo_ovqatga.homashyo_id')
                    ->leftJoin('ovqatlar', 'ovqatlar.id = homashyo_ovqatga.ovqat_id')
                    ->where(['in', 'homashyo_id', $ids])
                    ->andWhere(['ovqatlar.status' => '1'])
                    ->groupBy('homashyo_ovqatga.ovqat_id')
                    ->having('count(homashyo_id) >= 2')
                    ->orderBy(['ingCount' => SORT_DESC])
                    ->asArray()
                    ->all();
                $model->pr['ovqat'] = $ovqatlarVaHom;
                $model->pr['options'] = ArrayHelper::map($ings,'id','name');
                $model->pr['ids'] = $ids;
                return $this->render('index', ['model' => $model]);


            }

            $model->pr['ovqat'] = $ovqatlarVaHom;
            $model->pr['options'] = ArrayHelper::map($ings,'id','name');
            $model->pr['ids'] = $ids;

        return $this->render('index', ['model' => $model]);

    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
