<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\HomashyoOvqatga;
use Yii;
use app\modules\admin\models\Ovqatlar;
use app\modules\admin\models\OvqatlarSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OvqatlarController implements the CRUD actions for Ovqatlar model.
 */
class OvqatlarController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Ovqatlar models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OvqatlarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Ovqatlar model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Ovqatlar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->can('ovqatlar/insert')){
            $model = new Ovqatlar();
            $ingsToFood = new HomashyoOvqatga();

            if ($model->load(Yii::$app->request->post()) ) {
                debug($model); exit();
                $model->save();
                return $this->redirect(['view', 'id' => $model->id]);
            }

            return $this->render('ovqatlar/create', [
                'model' => $model,
            ]);
        }else{
            throw new ForbiddenHttpException('Sizga Taom qo\'shishga ruhsat berilmagan');
        }


    }

    /**
     * Updates an existing Ovqatlar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->can('ovqatlar/update')){
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }

            return $this->render('ovqatlar/update', [
                'model' => $model,
            ]);
        }else{
            throw new ForbiddenHttpException('Sizga Taom yangilashga ruhsat berilmagan');
        }

    }

    /**
     * Deletes an existing Ovqatlar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->can('ovqatlar/delete')){
            $this->findModel($id)->delete();



            return $this->redirect(['index']);
        }else{
            throw new ForbiddenHttpException('Sizga Taom o\'chirishga ruhsat berilmagan');

        }


    }

    /**
     * Finds the Ovqatlar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ovqatlar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ovqatlar::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
