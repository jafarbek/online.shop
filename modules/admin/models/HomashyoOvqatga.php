<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "homashyo_ovqatga".
 *
 * @property int $id
 * @property int $homashyo_id
 * @property int $ovqat_id
 *
 * @property Homashyolar $homashyo
 * @property Ovqatlar $ovqat
 */
class HomashyoOvqatga extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'homashyo_ovqatga';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['homashyo_id', 'ovqat_id'], 'required'],
            [['homashyo_id', 'ovqat_id'], 'integer'],
            [['homashyo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Homashyolar::className(), 'targetAttribute' => ['homashyo_id' => 'id']],
            [['ovqat_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ovqatlar::className(), 'targetAttribute' => ['ovqat_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'homashyo_id' => 'Homashyo ID',
            'ovqat_id' => 'Ovqat ID',
        ];
    }

    /**
     * Gets query for [[Homashyo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHomashyo()
    {
        return $this->hasOne(Homashyolar::className(), ['id' => 'homashyo_id']);
    }

    /**
     * Gets query for [[Ovqat]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOvqat()
    {
        return $this->hasOne(Ovqatlar::className(), ['id' => 'ovqat_id']);
    }
}
