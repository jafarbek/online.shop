<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "homashyolar".
 *
 * @property int $id
 * @property string $name
 * @property string $status
 *
 * @property HomashyoOvqatga[] $homashyoOvqatgas
 */
class Homashyolar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'homashyolar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'status'], 'required'],
            [['status'], 'string'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'status' => 'Status',
        ];
    }

    /**
     * Gets query for [[HomashyoOvqatgas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHomashyoOvqatgas()
    {
        return $this->hasMany(HomashyoOvqatga::className(), ['homashyo_id' => 'id']);
    }

    public function getOvqatlar()
    {
        return $this->hasMany(Ovqatlar::className(), ['id' => 'ovqat_id'])->viaTable('homashyo_ovqatga', ['homashyo_id' => 'id']);
    }
}
