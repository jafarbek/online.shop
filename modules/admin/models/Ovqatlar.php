<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "ovqatlar".
 *
 * @property int $id
 * @property string $name
 * @property string $status
 *
 * @property HomashyoOvqatga[] $homashyoOvqatgas
 */
class Ovqatlar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    public $arrayIng;
    public static function tableName()
    {
        return 'ovqatlar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'status'], 'required'],
            [['status'], 'string'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'status' => 'Status',
        ];
    }

     public static function getListHomshyo(){

        $ings = Homashyolar::find()
            ->asArray()
            ->all();

        return $ings;
     }
    /**
     * Gets query for [[HomashyoOvqatgas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHomashyoOvqatga()
    {
        return $this->hasMany(HomashyoOvqatga::className(), ['ovqat_id' => 'id']);
    }


    public function getHomashyolar()
    {
        return $this->hasMany(Homashyolar::className(), ['id' => 'homashyo_id'])->viaTable('homashyo_ovqatga', ['ovqat_id' => 'id']);
    }
}
