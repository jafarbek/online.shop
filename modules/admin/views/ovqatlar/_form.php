<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Ovqatlar */
/* @var $form yii\widgets\ActiveForm */
?>
<?php //debug($model::getListHomshyo());?>
<div class="ovqatlar-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([ '0', '1', ], ['prompt' => '']) ?>

<!--    --><?//= $form->field($ingsToFood, 'homashyo_id')->widget(Select2::className(),[
//        'data' => \yii\helpers\ArrayHelper::map($model::getListHomshyo(), 'id', 'name'),
//        'options' => [
//            'multiple' => true
//        ]
//    ])?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
