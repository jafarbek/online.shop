<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Ovqatlar */

$this->title = 'Create Ovqatlar';
$this->params['breadcrumbs'][] = ['label' => 'Ovqatlars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ovqatlar-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
