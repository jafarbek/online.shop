<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\AuthAssignment;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AuthAssignmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Auth Assignments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-assignment-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Auth Assignment', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                    'attribute' => 'item_name',
                    'label' => 'Berilgan dostup'
            ],

            [
                'label' => 'Foydalanuvchi',
                'attribute' => 'user_id',
                'format' => 'raw',
//                'filter' => \yii\helpers\ArrayHelper::map(AuthAssignment::userList(),'id','username'),
                'value' => function($model){
                    if($model->user)
                        return $model->user->username;
                    return "";
                }
            ],
            'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
