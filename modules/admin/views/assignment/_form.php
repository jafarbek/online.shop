<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\AuthAssignment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auth-assignment-form">
<?php //debug($model::userList()); ?>
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'item_name')->widget(Select2::className(), [
        'data'=>\yii\helpers\ArrayHelper::map($model::getAccesses(), 'item_name','item_name' )
    ])->label('Beriladigan ruhsat') ?>

    <?= $form->field($model, 'user_id')->widget(Select2::className(), [
            'data'=>\yii\helpers\ArrayHelper::map($model::userList(), 'id', 'username'),
        'options' => ['placeholder' => 'Select a state ...'],
    ] )->label('Foydalanuvchi') ?>

<!--    --><?//= $form->field($model, 'created_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
