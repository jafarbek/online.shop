<?php
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Html;
    use kartik\select2\Select2;
    use yii\helpers\Url;
/* @var $model app\models\SearchForm */



    ?>

<div class="container">
    <div class="row">
        <?php  $form = ActiveForm::begin(['method' => 'get',
            'action' => Url::to(['index'])
        ]); ?>
        <?=$form->field($model, 'ings')->widget(Select2::className(),[
            'data' => $model->pr['options'],
            'options' => [
                    'multiple' => true
            ]
        ])?>

        <?=Html::submitButton('search', ['class'=>'btn btn-primary'])?>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<?php if (isset($model->pr['ovqat'])) :?>
<div class="container">
    <div class="row">
        <?php foreach ($model->pr['ovqat'] as $item) :?>
        <div class="alert alert-info">
            <h1 ><?= $item['name'] ?></h1>
        <h5 > Mos kelgan ingredientlar soni:  <?= $item['ingCount'] ?></h5>
        </div>
        <?php endforeach; ?>
    </div>
</div>
<?php endif; ?>